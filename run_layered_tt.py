import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
from seismiclib import XT_given_p_range
from model_graphics import *

df = pd.read_csv("FA1.txt", sep=';', header=0, names=["Z", "VP", "VS", "RHO"])

# plot the model for reference
plt.figure()
plt.gca().invert_yaxis()
plot_model_label(df, "VP", "(km/s)")
plt.savefig("FA1_VP.png")

nrays = 1000

# calculate rays using standard formula

p = np.linspace(0.1236, 0.2217, nrays)
parray, Xarray, Tarray = XT_given_p_range(df, p, "VP")

# do plotting

plt.figure()
plot_tt_curve(Xarray, Tarray)
plt.savefig("FA1_XT.png")

plt.figure()
plot_tt_curve(Xarray, Tarray, vred=8.1)
plt.savefig("FA1_XT_vred.png")

plt.figure()
plot_Xp_curve(parray, Xarray)
plt.savefig("FA1_pX.png")

plt.figure()
plot_Tp_curve(parray, Tarray)
plt.savefig("FA1_pT.png")

plt.figure()
plot_taup_curve(parray, Xarray, Tarray)
plt.savefig("FA1_taup.png")


