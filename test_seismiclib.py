import numpy as np
from seismiclib import *

def test_layerXT_uniform_vertical():

    p = 0 # vertical ray
    h = 30 # 30km thick  crust
    utop = 1 / 6. # crustal P-wave velocity
    ubot = utop

    dx, dt, irtr = layerXT(p, h, utop, ubot)
    assert dx == 0
    assert dt == h * utop
    assert irtr == 1

def test_layerXT_uniform_45deg():

    h = 30 # 30km thick  crust
    utop = 1 / 6. # crustal P-wave velocity
    ubot = utop
    p = utop * np.sin(45 * np.pi / 180.) # ray at 45 degrees
    print p

    dx, dt, irtr = layerXT(p, h, utop, ubot)
    assert np.abs(dx - h) < 0.0001 
    assert dt == dx * np.sqrt(2) * utop
    assert irtr == 1
