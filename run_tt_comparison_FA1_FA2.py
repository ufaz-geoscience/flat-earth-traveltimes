import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
from seismiclib import XT_given_p_range
from model_graphics import *

df1 = pd.read_csv("FA1.txt", sep=';', header=0, names=["Z", "VP", "VS", "RHO"])
df2 = pd.read_csv("FA2.txt", sep=';', header=0, names=["Z", "VP", "VS", "RHO"])

# plot the model for reference
plt.figure()
plt.gca().invert_yaxis()
plot_model_label(df1, "VP", "(km/s)", color="blue")
plot_model_label(df2, "VP", "(km/s)", color="red")
plt.savefig("comp_FA1_FA2_VP.png")

nrays = 1000
vred = 8.1

# calculate rays using standard formula

p = np.linspace(0.1236, 0.2217, nrays)
parray1, Xarray1, Tarray1 = XT_given_p_range(df1, p, "VP")
parray2, Xarray2, Tarray2 = XT_given_p_range(df2, p, "VP")


# do tt plotting

plt.figure()
plot_tt_curve(Xarray1, Tarray1, color="blue")
plot_tt_curve(Xarray2, Tarray2, color="red")
plt.savefig("comp_FA1_FA2_XT.png")

plt.figure()
plot_tt_curve(Xarray1, Tarray1, color="blue", vred=vred)
plot_tt_curve(Xarray2, Tarray2, color="red", vred=vred)
plt.savefig("comp_FA1_FA2_XT_vred.png")

