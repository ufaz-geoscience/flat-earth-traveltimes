# Flat Earth Travel-Times

Here you will find instructional Python code that will help you calculate
travel-time curves for Earth structure that can be approximated as a series of
horizontal layers with constant velocities or linear gradients in seismic
velocities.

Earth models are specified in the .txt files as a series of depths, followed by
the P and S wave velocities at those depths and the densities. To indicate a
constant velocity layer, use the same velocities at the top and bottom of the
layer. To indicate a sharp interface, use the same depth twice, once with the
velocities above the interface and once with the velocities below the
interface.

The main program - run_layered_tt.py - reads in an Earth model file then plots
the P and S wave velocities as a function of depth, calculates the P-wave
travel-time curve between two values of the ray parameter (p), and plots it
using the chosen reduction velocity. 
