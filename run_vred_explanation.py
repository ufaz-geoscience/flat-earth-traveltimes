import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
from seismiclib import XT_given_p_range
from model_graphics import *

df = pd.read_csv("SHEARER_EX.txt", sep=';', header=0, names=["Z", "VP", "VS", "RHO"])

# plot the model for reference
plt.figure()
plt.gca().invert_yaxis()
plot_model_label(df, "VP", "(km/s)")
plt.savefig("SHEARER_EX_VP.png")

nrays = 1000
vred = 6.0

# calculate rays using standard formula

p = np.linspace(0.160, 0.42, nrays)
parray, Xarray, Tarray = XT_given_p_range(df, p, "VP")

# do plotting

plt.figure()
plot_tt_curve(Xarray, Tarray)
plt.savefig("SHEARER_EX_XT.png")

plt.figure()
plot_tt_curve(Xarray, Tarray)
plt.gca().set_ylim([0,6])
plt.gca().set_xlim([0,80])
plt.savefig("SHEARER_EX_XT_zoom.png")

plt.figure()
plot_tt_curve(Xarray, Tarray, vred=vred)
plt.savefig("SHEARER_EX_XT_vred.png")

plt.figure()
plot_tt_curve(Xarray, Tarray, vred=vred)
plt.gca().set_ylim([0,6])
plt.gca().set_xlim([0,80])
plt.savefig("SHEARER_EX_XT_vred_zoom.png")

plt.figure()
plot_Xp_curve(parray, Xarray)
plt.savefig("SHEARER_EX_pX.png")

plt.figure()
plot_Tp_curve(parray, Tarray)
plt.savefig("SHEARER_EX_pT.png")

plt.figure()
plot_taup_curve(parray, Xarray, Tarray)
plt.savefig("SHEARER_EX_taup.png")


