import numpy as np

def layerXT(p,h,utop,ubot):
    """
    Calculates dx and dt for a ray in a layer with a linear velocity gradient.
    This is a highly modified version of a subroutine in Chris Chapman's WKBJ
    program. This python version was translated by Alessia Maggi from the
    Fortran 77 version in Peter Shearer's Introduction to Seismology book.

    Inputs: 
        p = horizontal slowness
        h = layer thickness
        utop = sloness at top of layer
        ubot = slowness at bottom of layer
    Outputs:
        dx = range offset
        dt = travel time
        irtr = return code 
                = -1, zero thickness layer
                = 0, ray turned above layer
                = 1, ray passed through layer
                = 2, ray turned in layer, 1 leg counted in dx, dt
    """

    # if p > utop, then ray turned before getting to this layer
    if (p >= utop):
        dx = 0.
        dt = 0.
        irtr = 0
        return (dx, dt, irtr)
    # if h=0, then zero thickness layer (sharp interface)
    elif (h == 0):
        dx = 0.
        dt = 0.
        irtr = -1
        return (dx, dt, irtr)

    # for other cases
    u1 = utop
    u2 = ubot
    v1 = 1. / u1 # velocity at top of layer
    v2 = 1. / u2 # velocity at bottom of layer
    b = (v2-v1) / h # slope of velocity gradient

    eta1 = np.sqrt(u1**2 - p**2) # denominator in travel-time curve

    # constant velocity layer
    if (b == 0):
        dx = h * p / eta1
        dt = h * u1**2 / eta1
        irtr = 1
        return (dx, dt, irtr)

    x1 = eta1 / (u1 * p * b)
    tau1 = (np.log((u1 + eta1) / p) - eta1 / u1) / b

    # ray turns within layer --> no contribution to integral from bottom point
    if (p >= ubot):
        dx = x1
        dtau = tau1
        dt = dtau + p * dx
        irtr = 2
        return (dx, dt, irtr)

    # ray passes through layer
    irtr = 1
    eta2 = np.sqrt(u2**2 - p**2)
    x2 = eta2 / (u2 * b * p)
    tau2 = (np.log((u2 + eta2) / p) - eta2 / u2) / b
    dx = x1 - x2
    dtau = tau1 - tau2
    dt = dtau + p * dx
    return (dx, dt, irtr)

def XT_given_p(df, p, wavetype):

    ndepths = len(df["Z"])
    X = 0
    T = 0
    X_all = []
    T_all = []
    

    # sum contributions for all layers
    for layer in xrange(ndepths-1):
        h = df["Z"][layer+1] - df["Z"][layer]
        utop = 1./df[wavetype][layer]
        ubot = 1./df[wavetype][layer+1]
        (dx, dt, irtr) = layerXT(p, h, utop, ubot)
        X += dx
        T += dt
        if irtr == 2:
            continue
        if irtr == -1 and layer < ndepths-2: # can have a reflection
            X_all.append(X)
            T_all.append(T)

    
    # if last leg turns in last layer above the last layer, then also
    # retrun refracted ray X, T, else only return reflected rays
    if (irtr == 0 or irtr == 2) and T>0 :
        X_all.append(X)
        T_all.append(T)
    return X_all, T_all

def XT_given_p_range(df, p, wavetype):
    X = []
    T = []
    p_full = []

    nrays = len(p)

    # iterate over rays to get points on travel-time curve
    # DF1
    for i in xrange(nrays):
        X_out, T_out = XT_given_p(df, p[i], wavetype)
        X.extend(X_out)
        T.extend(T_out)
        p_full.extend([p[i] for x in X_out])

    Xarray = 2 * np.array(X)
    Tarray = 2 * np.array(T)
    parray = np.array(p_full)

    return parray, Xarray, Tarray


